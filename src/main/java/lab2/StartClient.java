package lab2;

import java.io.IOException;
import java.util.Scanner;

public class StartClient {
    public static void main(String[] args) throws IOException {
        Client client = new Client();
        client.startClient("127.0.0.1", 8080);
        System.out.print("Введите сообщение: ");
        Scanner scan = new Scanner(System.in);
        String message = scan.nextLine();
        String response = client.sendMessage(message);
        System.out.println(response);
    }
}
