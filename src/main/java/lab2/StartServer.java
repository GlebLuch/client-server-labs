package lab2;

import java.io.IOException;

public class StartServer {
    public static void main(String[] args) throws IOException {
        Server server = new Server();
        System.out.println("Сервер запущен...");
        server.startServer(8080);
    }
}
