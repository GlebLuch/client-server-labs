package lab3;

import java.io.*;
import java.net.Socket;

public class Client {
    public static void main(String[] args) throws IOException {
        System.out.println("Пользователь вошел в чат");
        Socket socket = new Socket("127.0.0.1", 8080);
        DataInputStream in = new DataInputStream(socket.getInputStream());
        DataOutputStream out = new DataOutputStream(socket.getOutputStream());

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String msg1 = "";
        String msg2;
        while (!msg1.equals("end")) {
            msg2 = reader.readLine();
            out.writeUTF(msg2);
            msg1 = in.readUTF();
            System.out.println(msg1);
        }
    }
}
