package lab3;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8080);
        Socket socket = serverSocket.accept();

        DataInputStream in = new DataInputStream(socket.getInputStream());
        DataOutputStream out = new DataOutputStream(socket.getOutputStream());

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String msg1 = "";
        String msg2;

        while (!msg1.equals("end")) {
            msg1 = in.readUTF();
            System.out.println(msg1);
            msg2 = reader.readLine();
            out.writeUTF(msg2);
            out.flush();
        }
        socket.close();
    }
}
