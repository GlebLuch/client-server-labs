package lab_2_popov;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class UDPServer {
    public static final int SERVICE_PORT = 50001;

    public static void main(String[] args) throws IOException {
        try (DatagramSocket serverSocket = new DatagramSocket(SERVICE_PORT)) {

            byte[] receivingDataBuffer = new byte[1024];
            byte[] sendingDataBuffer;

            DatagramPacket inputPacket = new DatagramPacket(receivingDataBuffer, receivingDataBuffer.length);
            System.out.println("Waiting for a client to connect...");

            serverSocket.receive(inputPacket);

            String receivedData = new String(inputPacket.getData());
            System.out.println("Сообщение от клиента: " + receivedData);

            String messageFromServer = "Сообщение от сервера с адресом " + serverSocket.getLocalSocketAddress() + ": "
                    + receivedData.toUpperCase();

            sendingDataBuffer = messageFromServer.getBytes();

            InetAddress senderAddress = inputPacket.getAddress();
            int senderPort = inputPacket.getPort();

            DatagramPacket outputPacket = new DatagramPacket(
                    sendingDataBuffer, sendingDataBuffer.length,
                    senderAddress, senderPort
            );
            serverSocket.send(outputPacket);
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }
}