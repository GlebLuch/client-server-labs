package lab_2_popov;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Scanner;

public class UDPClient {
    public static final int SERVICE_PORT = 50001;

    public static void main(String[] args) throws IOException {
        try (DatagramSocket clientSocket = new DatagramSocket()) {

            Scanner scan = new Scanner(System.in);

            InetAddress inetAddress = InetAddress.getByName("localhost");

            byte[] sendingDataBuffer;
            byte[] receivingDataBuffer = new byte[1024];

            System.out.print("Введите сообщение для сервера: ");
            String sentence = scan.nextLine();
            sendingDataBuffer = sentence.getBytes();

            DatagramPacket sendingPacket = new DatagramPacket(sendingDataBuffer, sendingDataBuffer.length, inetAddress, SERVICE_PORT);
            clientSocket.send(sendingPacket);
            DatagramPacket receivingPacket = new DatagramPacket(receivingDataBuffer, receivingDataBuffer.length);
            clientSocket.receive(receivingPacket);
            String receivedData = new String(receivingPacket.getData());
            System.out.println(receivedData);

        } catch (SocketException e) {
            e.printStackTrace();
        }
    }
}