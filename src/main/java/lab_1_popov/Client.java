package lab_1_popov;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    private static final String RESPONSE = "Фактиориал числа %s = %s";

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.print("Введите адрес сервера (по умолчанию : 127.0.0.1) : ");

        var address = scan.nextLine();

        if (address == null) {
            address = "127.0.0.1";
        }

        try (Socket socket = new Socket(address, 8080);
             DataInputStream inputStream = new DataInputStream(socket.getInputStream());
             DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream())) {
            System.out.print("Отправьте число серверу : ");
            var request = scan.nextInt();
            outputStream.write(request);
            System.out.println("отправлено серверу: " + request);
            var calculatedRequest = inputStream.readUTF();
            System.out.printf((RESPONSE) + "%n", request, calculatedRequest);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
