package lab_1_popov;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8080);
        System.out.println("Сервер ждет клиента...");

        try (Socket socket = serverSocket.accept();
             DataInputStream inputStream = new DataInputStream(socket.getInputStream());
             DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream())) {

            System.out.println("Новое соединение: " + socket.getLocalSocketAddress().toString());
            int response;

            response = inputStream.read();
            System.out.println("прислал клиент: " + response);
            var responseFactorial = calculateFactorial(response);
            outputStream.writeUTF(String.valueOf(responseFactorial));
            System.out.println("отправлено клиенту: " + responseFactorial);
            System.out.println("клиент отключился");
        }
    }

    static int calculateFactorial(int n) {
        int result = 1;
        for (int i = 1; i <= n; i++) {
            result = result * i;
        }
        return result;
    }
}
